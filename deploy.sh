#!/bin/bash
set -e
export ANSIBLE_HOST_KEY_CHECKING=False

PIP="/usr/bin/pip3"

check_python() {
  echo "Checking Python version..."
  which "${PIP}" >/dev/null || {
    echo "Requires pip to be installed!"
    return 1
  }
}

install_requirements() {
  echo "Installing requirements..."
  "${PIP}" install --quiet --user --upgrade -r requirements.txt
}


provision_nodes() {
  echo "Provisioning EC2 Instances..."
  ansible-playbook "instance-creator/main.yaml"
}

# deploy_ceph runs the main ceph-ansible playbook that will deploy
# a new ceph cluster on provisioned nodes. It depends on provision_nodes
# for a generated hosts.ini file which is generated at /tmp/hosts.ini location.
# To configure the vars of the playbook, edit ./group_vars and site.yml, located
# in the same director as 'deploy.sh'.
deploy_kube() {
  echo "Deploying Kube on EC2 Cluster..."
  cd kubespray
  ansible-playbook -i /tmp/hosts.ini -b -v cluster.yml --private-key=~/cephkey.pem --become --become-user=root --flush-cache
  cd ..
}

# add_osds will run 'add-osd' playbook from ceph-ansible infrastructure playbooks.
# This playbook will add newly provisioned instances as OSD nodes to the existing
# Ceph cluster. This function depends on 'provision_nodes' for a generated 'hosts.ini'
# same as 'deploy_ceph' function.
deploy_rook() {
  echo "Deploying Rook..."
  cp -r group_vars ceph-ansible/infrastructure-playbooks/
  cd ceph-ansible
  ansible-playbook -i /tmp/hosts.ini --private-key ~/.ssh/id_rsa infrastructure-playbooks/add-osd.yml
  cd ..
}


main() {
  check_python
  install_requirements
  provision_nodes
  #deploy_kube
}

main $@
