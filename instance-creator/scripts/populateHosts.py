import argparse
import boto3
import sys
from collections import defaultdict

from inventory import AnsibleInventoryFile


def populate_hosts(vpc_id):
    # Initialize boto3 EC2 client
    ec2 = boto3.resource("ec2", region_name='us-east-1')

    # Hosts file template
    template = (
        '{% for type, nodes in nodes.items() %}'
        '[{{ type }}]\n'
        '{% for node in nodes %}'
        '{{ node.alias }} ansible_host={{ node.ip_address }} ip={{ node.ip_address }} ansible_user={{ node.ssh_user }} ansible_python_interpreter=/usr/bin/python3\n'
        '{% endfor %}\n'
        '{% endfor %}\n'
    )

    # empty host lists should be there in the inventory or some
    # ceph-ansible playbooks will fail due to missing groups, when
    # modifying an existing cluster.
    hosts = {
        'all': [],
        'kube-master': [],
        'kube-node': [],
        'etcd': [],
    }

    # To filter out all running CEPH ec2 instances in the VPC, regardless of role
    filters = [
        {
            'Name': 'tag:application',
            'Values': ['ceph']
        },
        {
            'Name': 'vpc-id',
            'Values': [vpc_id]
        },
        {
            'Name': 'instance-state-name',
            'Values': ['running']
        },
    ]

    for instance in ec2.instances.filter(Filters=filters):
        all_tags = {}
        for tags in instance.tags:
            all_tags[tags['Key']] = tags['Value']

        ip_address = instance.private_ip_address

        try:
            ssh_user = all_tags['ssh_user']
        except KeyError:
            ssh_user = 'ubuntu'

        extra_args = []
        try:
            role = all_tags['role']
        except KeyError:
            sys.exit('Error: Tag "role" not defined on instance!')

        hosts[role].append(
            {
                'alias': 'ip-'+('-').join(ip_address.split('.')),
                'ip_address': ip_address,
                'ssh_user': ssh_user,
                'extra_args': ' '.join(extra_args)
            }
        )
        if role=='kube-master':
            hosts['etcd'].append(
                {
                    'alias': 'ip-'+('-').join(ip_address.split('.')),
                    'ip_address': ip_address,
                    'ssh_user': ssh_user,
                    'extra_args': ' '.join(extra_args)
                }
            )

    # Generate Ansible Inventory file and dump to stdout
    file = AnsibleInventoryFile(dict(hosts), template)
    file.dump()
    file.destroy()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Generate hosts file for ceph-ansible')
    parser.add_argument('vpc', help='VPC ID to filter instances from')

    args = vars(parser.parse_args())
    populate_hosts(
        vpc_id=args['vpc'], )
