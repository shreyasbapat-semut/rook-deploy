"""
Generates Inventory Files for Ansible
"""
import os
from tempfile import NamedTemporaryFile

import jinja2

class AnsibleInventoryFile():
    """
    Creates a temporary inventory file to be used by Ansible.

    Args:
        nodes (list/dict): A list of dictionaries or any other data structure,
            containing a mapping of fields as required by the template.
        template (str): A Jinja2 template string that can parse the given ``nodes``.

    Note:
        If you use the following template for your inventory file::

            {% for type, nodes in nodes.items() %}
            [{{ type }}]\n
            {% for node in nodes %}
            {{ node.ip_address }} ansible_user={{ node.username }}\n
            {% endfor %}
            {% endfor %}

        Then you can pass ``nodes`` as a list of dictionaries::

            nodes = {
                        'metadata': [
                            {
                                'ip_address': '192.168.0.1',
                                'username': 'user'
                            },
                            {
                                'ip_address': '192.168.0.1',
                                'username': 'user'
                            },
                        ]
                        'manager': [],
                        'monitor': [],
                        'data': [],
                    ]

        This works because ``jinja2_template.render(nodes)`` is called under the hood.

    Example:
        >>> nodes = [{'ip_address': 192.168.0.1,'username': user}]
        >>> inventory_file = AnsibleInventoryFile(nodes)
        >>> print(inventory_file.path)

    Warning:
        Dont't forget to call :func:`destroy()` after the inventory
        file is no longer required.
    """

    def __init__(self,
                 nodes,
                 template=(
                     '[nodes]\n'
                     '{% for node in nodes %}'
                     '{{ node.ip_address }} ansible_user={{ node.username }}\n'
                     '{% endfor %}')
                 ):
        self.nodes = nodes
        self.template = template

        jinja2_template = jinja2.Template(
            self.template, lstrip_blocks=True, trim_blocks=True)
        inventory = jinja2_template.render(nodes=self.nodes)
        self.invm = inventory + '\n[k8s-cluster:children]\nkube-node\nkube-master\n'
        self.inventory_file = NamedTemporaryFile(mode='w+t', delete=False)
        self.inventory_file.write(self.invm)
        self.inventory_file.close()

    @property
    def path(self):
        """Get the system path of the temporary inventory file."""
        return self.inventory_file.name

    def dump(self):
        """Prints the contents of inventory file to stdout."""
        try:
            with open(self.inventory_file.name, 'r') as fin:
                print(fin.read())
        except (NameError, FileNotFoundError):
            return None

    def destroy(self):
        """Deletes the temporary file from OS"""
        os.remove(self.inventory_file.name)
