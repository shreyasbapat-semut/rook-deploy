# Configuring vars

## Common

Fields `region`, `vpc_id`, `cluster_name` and `azs` are required.

Each entry in `azs` specifies a CIDR block and an availability zone to
create appropriate subnets. When specifying the nodes config, the subnet
ID will be inferred from the `azs` name.

`ec2` specifies default values to use for all nodes when these aren't
specified in individual vars.

## Ceph Nodes

Each kind of Ceph node has its own configuration file.
Based on the operation specified in `deploy.sh`, e.g.
`deploy.sh osds`, the script will load `common.yaml` and
operation specific vars, e.g. `osds.yaml` and run the
appropirate tasks.

In each node related vars file, `count` will create that
many instances in each of the given AZs. AZs here accepts
the 'zones' from common config (which creates subnets for each zone)
or we can specify an actual subnet_id here, e.g.

```yaml
azs: ["us-west-2a", "subnet-0a949b6c56f489587"]
```

Above will create instances in both subnets.

By default, the playbook creates and attaches a private security
group to every instance, but any additional security groups declared
here will also be added to the list (playbook doesn't create these group
so they need to be present before running this)

```yaml
security_groups: ["blahblah"]
```

_Note: Most of the variables specified will override values given in `ec2`
dict in `common.yaml`._
