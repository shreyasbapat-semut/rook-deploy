## Deployment

### 1. Clone the repository

```bash
git clone git@gitlab.com/semut-technologies/rook-deploy.git
```

### 2. Install Python 3 and Pip 3

```bash
yum install -y python36 python36-pip
```

- Once installed, edit `PIP` var in `deploy.sh` to reflect correct `pip3` binary.

### 3. Edit config

- Add AWS credentials to `~/.aws/credentials`

```ini
[default]
aws_access_key_id = YOUR_KEY
aws_secret_access_key = YOUR_SECRET
```

- Edit config for EC2 cluster as mentioned in [Instance Creator docs][1]

### 4. Follow [Deploying][2]


[1]: EC2-Provisioner
[2]: Deploying
